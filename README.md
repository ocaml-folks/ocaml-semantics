# Ocaml Semantics

The purpose of this repository is to exchange ideas, opinions and
definitions for the semantics of subsets of the OCaml language or of
some intermediate languages for OCaml.

# Organisation

- `salto-IL`: definitions related to the intermediate language used by
  the [Salto](https://salto.gitlabpages.inria.fr/) static analyser
