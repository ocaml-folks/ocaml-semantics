\documentclass[a4paper,twoside,11pt]{article}
\usepackage[scale=0.75]{geometry}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[british]{babel}

\usepackage{amsthm}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{pifont}

\usepackage{mathpartir}

\usepackage{xspace}
\usepackage{booktabs}
\usepackage{microtype}

\usepackage{minted}

\usepackage{xxx}

\usepackage{hyperref}
\usepackage{csquotes}

\usepackage{biblatex}
\addbibresource{references.bib}

\overfullrule=0.5cm                                   % prints overfull hboxes

\usepackage{macros}

\newcommand{\copyrightnotice}{Copyright \copyright{} Inria 2023\xspace}

\begin{document}

\title{\salto's Intermediate Language: Syntax and Semantics}
\author{Benoît Montagu \and Pierre Lermusiaux}
\date{Version \texttt{0.1} --- November 2023\\
  \copyrightnotice}

\maketitle


The purpose of this document is to introduce the syntax and
operational semantics of (a simplified version of) the intermediate
language that is used by the
\href{https://salto.gitlabpages.inria.fr/}{\salto} static analyser for
\ocaml programs \cite{Leroy2023}.

The goal of this document is to serve as a reference for the \salto
intermediate language and to serve as a basis for technical
discussions.

\section{Versions}
\label{sec:versions}

\begin{itemize}
\item \texttt{0.1} (November 2023): initial version
\end{itemize}

\section{Limitations}
\label{sec:limitations}

The current version of the document does not cover the following aspects:
\begin{itemize}
\item Source file positions,
\item Guards in patterns,
\item Bytes/Arrays,
\item Laziness,
\item General recursive definitions,
\item The module language \cite{Leroy2000},
\item The class/object system,
\item Polymorphic equality/comparison,
\item Physical equality,
\item Memory representation of data,
\item Unsafe features (\eg, \texttt{\%identity} external),
\item Garbage collection (finalisers, \etc),
\item Modelling of resources (\eg, memory, stack),
\item System state and system calls,
\item \posix signals,
\item Relaxed memory model,
\item Concurrency/parallelism/multicore,
\item Algebraic effects.
\end{itemize}

\clearpage

\section{Syntax}
\label{sec:syntax}

\begin{definition}[Constructors]
  The set of constructors $\CC$ is an infinite, denumerable set.
  Elements of $\CC$ are written $C$.
\end{definition}

Constructors also encompass the unit value, booleans, tuples and
immutable records.

\begin{definition}[Base values]
  The set of base values $\BASEVALUES$ contains signed
  31-bit/32-bit/63-bit/64-bit integers, \texttt{ASCII} characters,
  strings and floats. Elements of $\BASEVALUES$ are written $b$.
\end{definition}

\begin{definition}[Patterns]
  \[\begin{array}{rcll}
    p \in \PATTERNS &::=
            & x & \text{(Variables)}
              \ORCR b & \text{(Base value literals)}
              \ORCR C(p_1,\ldots,p_k) & \text{(Constructs)}
              \ORCR \dync{x}(p_1,\ldots,p_k) & \text{(Dynamic constructs)}
              \ORCR {p_1} \plus {p_2} & \text{(Union)}
              \ORCR {p_1} \minus {p_2} & \text{(Complement)}
              \ORCR \bot & \text{(Empty)}
    \end{array}\]
\end{definition}

\begin{remark}
  In the pattern $\dync{x}(p_1,\ldots,p_k)$, the variable $x$ is a
  free variable (that should be queried in an environment), but does
  not bind. All the other variables that occur in patterns are
  \emph{binding} variables.
\end{remark}

\begin{remark}
  Patterns are supposed to be well formed, in the following sense:
  \begin{itemize}
  \item In $C(p_1,\ldots,p_k)$, the patterns $p_1,\dots,p_k$ must
    mention pairwise distinct variables.
  \item In $\dync{x}(p_1,\ldots,p_k)$, the patterns
    $p_1,\dots,p_k$ must mention pairwise distinct variables.
  \item In ${p_1} \plus {p_2}$, the patterns $p_1$ and $p_2$ must
    mention exactly the same variables.
  \item In ${p_1} \minus {p_2}$, there is no constraints between the
    variables of the patterns $p_1$ and $p_2$.
  \end{itemize}
\end{remark}

\vfill

\begin{definition}[Terms]
  \[\begin{array}{rcll}
    t,u \in \TERMS &::=
            & x & \text{(Variables)}
              \ORCR b & \text{(Base value literals)}
              \ORCR \expcallprim{p}{x_1,\dots,x_n} & \text{(Call to primitive)}
              \ORCR C(x_1,\ldots,x_n) & \text{(Constructs)}
              \ORCR \dync{x}(x_1,\ldots,x_n) & \text{(Dynamic constructs)}
              \ORCR \dync{x} & \text{(Extension constructors)}
              \ORCR \lam{x}{t} & \text{(Abstraction)}
              \ORCR f~x_1~\dots~x_n & \text{(Application ($n$-ary))}
              \ORCR \expletrec{f}{x}{t}{u} & \text{(Recursive function)}
              \ORCR \explets{x_1}{t_1}{\dots~\expand{x_n}{t_n}}{u} & \text{(Let bindings)}
              \ORCR \dispatch{t}~\dval{x_1}{u_1} \,|\, \dexn{x_2}{u_2}
      & \text{(Exception dispatch)}
              \ORCR \expassert{t} & \text{(Assertion)}
              \ORCR \expif{t}{u_1}{u_2} & \text{(Conditional)}
              \ORCR \matchwith{t}~ p_1 \earrow u_1 \,|\,\cdots\,|\, p_n \earrow u_n  & \text{(Pattern matching)}
              \ORCR \unreachable & \text{(Unreachable)}
              \ORCR \letdync{x}{t}
      & \text{(Extension constructor creation)}
              \ORCR \letdyncalias{x}{y}{t}
      & \text{(Extension constructor alias)}
              \ORCR \{ f_1 = x_1; \dots; f_n = x_n \} & \text{(Mutable record creation)}
              \ORCR x.f & \text{(Mutable record access)}
              \ORCR x.f \leftarrow y  & \text{(Mutable record update)}
              \ORCR \expfor[(to|downto)]{x}{y}{z}{t} & \text{(For loop)}
              \ORCR \expwhile{t}{u} & \text{(While loop)}
    \end{array}\]
\end{definition}

\begin{remark}
  In loops of the form $\expfor[(to|downto)]{x}{y}{z}{t}$, we enforce
  that $y \neq z$ and that $y$ is not a free variable of $t$.
\end{remark}

The main differences between our intermediate language and \ocaml
\texttt{Typedtree}, is that we deal with only one construct for
pattern matching, and only one construct for exception handling. This
is in contrast with the constructs
\begin{center}
\mintinline{ocaml}{try t1 with p -> t2} and
\end{center}
and
\begin{center}
\mintinline{ocaml}{match t with p1 -> t1 | exception p2 -> t2}
\end{center}
that conflate pattern matching with exception handling. The
transformation into our two constructs is mostly straightforward, but
greatly simplifies the job of the static analyser.

The syntax of our intermediate language makes explicit the evaluation
order with \textsf{let} bindings. While the evaluation order in \ocaml
is generally unspecified, we did our best to mimic the choices that
the \ocaml compiler makes later at \textsf{Lambda} level of its
compilation pipeline.

We added specific application nodes for primitives. This required to
guarantee the (minimal) arity of these primitives, and insert
$\lambda$-abstractions when they were partially applied, or insert
more application nodes when they were given more arguments than
expected. We also needed to handle specially the short-circuiting
boolean expressions---that are regular \ocaml primitives---because
they change the evaluation order.

We kept the $n$-ary application nodes of the \ocaml AST (instead of
the binary applications, as this is important for the semantics of
labelled/optional function arguments. Nevertheless, the transformation
from the \ocaml AST into our intermediate language needed a lot of
care and effort. In particular, missing labelled arguments required
the insertion of $\lambda$-abstractions, and the interaction between
missing labelled arguments and optional arguments was particularly
subtle. The evaluation order that is implemented by the \ocaml
compiler in such cases is, indeed, surprisingly complex. See, for
instance,
\href{https://github.com/ocaml/ocaml/issues/10652}{\texttt{Issue
    \#10652}} and
\href{https://github.com/ocaml/ocaml/pull/10653}{\texttt{PR \#10653}}.

\clearpage
\section{Operational Semantics}
\label{sec:semantics}

This section defines an operational semantics for the programming
language of section~\ref{sec:syntax}. It is defined as a big-step
environment-based call-by-value operational semantics.

A particularity of the semantics is that it assumes that the patterns
that occur in a pattern matching construct must be \emph{unambiguous}
and \emph{exhaustive}: this means that for any value of the expected
type, exactly one pattern matches that value. This is explained in
section~\ref{sec:sem-pattern-matching}.

The evaluation judgement are of the form
$\bstepctxgen[K]{m}{\STATE}{\EVALENV}{t}{\STATE'}{v}$, where:
\begin{itemize}
\item $K$ is the \emph{kind} of the judgement, that distinguishes
  between the semantics of different parts of the language:
  \begin{itemize}
  \item $\kindCore$ stands for \emph{core language},
  \item $\kindPrim$ stands for \emph{primitives} (akin to
    \emph{externals} in \ocaml),
  \item $\kindMod$ stands for \emph{module language} (not yet used).
  \end{itemize}
\item $m$ stands for the evaluation \emph{mode}:
  \begin{itemize}
  \item $\modeval$ stands for \emph{value mode}: this means that the
    evaluation produced a value.
  \item $\modeexn$ stands for \emph{exception mode}: this means that
    the evaluation raised an exception.
  \end{itemize}
\item $\STATE$ is a semantic state (see
  section~\ref{sec:semantics-preliminary-defs}), that represents the
  global, mutable state of the system. It is taken as input, and
  produced as output.
\item $\EVALENV$ is an environment (see
  section~\ref{sec:semantics-preliminary-defs}). It is used in a read
  only manner, and its use follows a stack discipline.
\item $t$ is a program when the kind of the judgement is $\kindCore$.
  It is a primitive name and a list of parameters if the kind of the
  judgement is $\kindPrim$.
\item $v$ is a value. If the mode is $\modeval$, then $v$ is the
  produced value (see Definition~\ref{def:values}). If the mode is
  $\modeexn$, then $v$ is the raised exception.
\end{itemize}

\begin{remark}
  The evaluation judgement for the core language ($\kindCore$ kind) is
  parameterised by the judgement for primitives ($\kindPrim$ kind).
\end{remark}

\begin{remark}
  Every primitive in this language has an eager semantics. In
  particular, the \ocaml externals \texttt{\%seqor} and
  \texttt{\%sequand} (for the short-circuiting boolean operators
  \texttt{||} and \texttt{\&\&}, respectively) are \emph{not}
  primitives of the language. They have been desugared into \textsf{if
    then else} constructs in a prior stage.
\end{remark}

\subsection{Preliminary definitions}
\label{sec:semantics-preliminary-defs}

\begin{definition}[Memory locations]
  The set of memory locations $\LOCS$ is an infinite, denumerable set.
  Elements of $\LOCS$ are written $\loc$.
\end{definition}

\begin{definition}[Extension constructors]
  The set of extension constructors $\NAMES$ is an infinite,
  denumerable set. Elements of $\NAMES$ are written $\name$.
\end{definition}

Values are defined as follows. Values contain closures, as is standard
in environment-based semantics. We consider that every closure is
potentially recursive.
\begin{definition}[Values]
\label{def:values}
\[\begin{array}{rcll}
    v \in \VALUES &::=
    & b & \text{(Base values)}
      \ORCR \loc & \text{(Memory locations)}
      \ORCR \name & \text{(Extension constructors)}
      \ORCR C(v_1,\ldots,v_k) & \text{(Constructs)}
      \ORCR \dync{\name}(v_1,\ldots,v_k) & \text{(Dynamic constructs)}
      \ORCR \closure{\EVALENV}{\reclam{f}{x}{t}}
      \quad \text{where $\dom \EVALENV = \fv (\lam{x}{t}) \setminus \{f\}$}
    & \text{(Closure)}
    \\
    \EVALENV \in \EVALENVS &::= & [] ~\mid~ \EVALENV, \xctx{x}{v}
        & \text{(Evaluation environments)}
  \end{array}\]
\end{definition}

\begin{definition}[States]
  \[\begin{array}{rcll}
      \STATE \in \STATES & ::= & (\HEAP, \NAMESET) & \text{(States)} \\
      \HEAP \in \HEAPS & ::= & \{ \loc_1 \mapsto \BLOCK_1, \dots, \loc_k \mapsto \BLOCK_k \} & \text{(Heaps)} \\
      \BLOCK & ::= & \{ f_1 \mapsto v_1, \dots, f_k \mapsto v_k \} & \text{(Heap blocks)} \\
      \NAMESET & ::= & \{ \name_1, \dots, \name_k \} & \text{(Sets of names)} \\
    \end{array}
  \]
\end{definition}

Semantic states are composed of a heap $\HEAP$ and a finite set of
names $\NAMESET$.
\begin{itemize}
\item The sets $\NAMESET$ denote the extension constructors that have
  been generated already.
\item Heaps $\HEAP$ are finite mappings from memory locations to heap
  blocks. Heap blocks are finite mappings from field names to values,
  and denote the contents of mutable records.
\end{itemize}

In the remaining of this document, we write $\uplus$ to denote the
disjoint union of sets or of finite maps. Furthermore, when
$\STATE = (\HEAP_0, \NAMESET_0)$, we use the following notations:
\begin{itemize}
\item $\STATE \uplus \NAMESET$ to denote
  $(\HEAP_0, \NAMESET_0 \uplus \NAMESET)$
\item $\STATE \uplus \HEAP$ to denote
  $(\HEAP_0 \uplus \HEAP, \NAMESET_0)$
\item $\STATE(\loc)$ to denote $\HEAP_0(\loc)$
\end{itemize}


\vfill

\subsection{Main evaluation rules for the core language}
\label{sec:sem-core}

\begin{mathpar}
  \inferrule[Var]
  { }
  {\bstepctx{\STATE}{\EVALENV}{x}{\STATE}{\EVALENV(x)}}

  \inferrule[Lit]
  { }
  {\bstepctx{\STATE}{\EVALENV}{b}{\STATE}{b}}

  \inferrule[Prim]
  {\bstepctxgen[\kindPrim]{m}
    {\STATE}
    {\EVALENV}
    {\expcallprim{p}{x_1,\dots, x_n}}
    {\STATE}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\expcallprim{p}{x_1,\dots, x_n}}
    {\STATE}
    {v}
  }

  \inferrule[Construct]
  {\forall 1 \leq i \leq n, \quad \EVALENV(x_i) = v_i }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {C(x_1,\ldots,x_k)}
    {\STATE}
    {C(v_1,\ldots,v_n)}
  }

  \inferrule[DynConstruct]
  {\EVALENV(y) = \name \\
    \forall 1 \leq i \leq n, \quad \EVALENV(x_i) = v_i
  }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\dync{y}(x_1,\ldots,x_k)}
    {\STATE}
    {\dync{\name}(v_1,\ldots,v_n)}
  }

  \inferrule[ExtConstructor]
  {\EVALENV(x) = \name}
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\dync{x}}
    {\STATE}
    {\name}
  }

  \inferrule[Lam]
  {\EVALENV' = \restrict{\EVALENV}{\fv(\lam{x}{t})} \\ f \notin \fv(\lam{x}{t}) }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\lam{x}{t}}
    {\STATE}
    {\closure{\EVALENV'}{\reclam{f}{x}{t}}}
  }

  \inferrule[App]
  {\EVALENV(y) = \closure{\EVALENV_1}{\reclam{f_1}{x_1}{t_1}}
    \\\\
    \forall 1 \!\leq\! i \!<\! n, \;
    \bstepctx
    {\STATE_i}
    {\EVALENV_i,
      \xctx{f_i}{\closure{\EVALENV_i}{\reclam{f_i}{x_i}{t_i}}},
      \xctx{x_i}{\EVALENV(z_i)}
    }
    {t_{i+1}}
    {\STATE_{i+1}}
    {\closure{\EVALENV_{i+1}}{\reclam{f_{i+1}}{x_{i+1}}{t_{i+1}}}}
    \\\\
    \bstepctx
    {\STATE_n}
    {\EVALENV_n,
      \xctx{f_n}{\closure{\EVALENV_n}{\reclam{f_n}{x_n}{t_n}}},
      \xctx{x_n}{\EVALENV(z_n)}
    }
    {t_{n+1}}
    {\STATE_{n+1}}
    {v}
  }
  {\bstepctxgen{m}{\STATE_1}{\EVALENV}{y~z_1~\dots~z_n}{\STATE_{n+1}}{v}
  }

  \inferrule[AppRaise]
  {\EVALENV(y) = \closure{\EVALENV_1}{\reclam{f_1}{x_1}{t_1}} \\
    1 \leq k \leq n
    \\\\
    \forall 1 \!\leq\! i \!<\! k, \;
    \bstepctx
    {\STATE_i}
    {\EVALENV_i,
      \xctx{f_i}{\closure{\EVALENV_i}{\reclam{f_i}{x_i}{t_i}}},
      \xctx{x_i}{\EVALENV(z_i)}
    }
    {t_{i+1}}
    {\STATE_{i+1}}
    {\closure{\EVALENV_{i+1}}{\reclam{f_{i+1}}{x_{i+1}}{t_{i+1}}}}
    \\\\
    \throwctx
    {\STATE_k}
    {\EVALENV_k,
      \xctx{f_k}{\closure{\EVALENV_k}{\reclam{f_k}{x_k}{t_k}}},
      \xctx{x_k}{\EVALENV(z_k)}
    }
    {t_{k+1}}
    {\STATE_{k+1}}
    {v}
  }
  {\throwctx{\STATE_1}{\EVALENV}{y~z_1~\dots~z_n}{\STATE_{k+1}}{v}
  }

  \inferrule[LetRec]
  {\EVALENV' = \restrict{\EVALENV}{\fv(\lam{x}{t}) \setminus \{f\}} \\\\
    \bstepctxgen{m}{\STATE}{\EVALENV,\xctx{x}{\closure{\EVALENV'}{\reclam{f}{x}{t}}}}{u}{\STATE'}{v}
  }
  {\bstepctxgen{m}{\STATE}{\EVALENV}{\expletrec{f}{x}{t}{u}}{\STATE'}{v}}

  \inferrule[LetAnd]
  {\forall 0 \leq i \leq n,\quad
    \bstepctx
    {\STATE_{i-1}}
    {\EVALENV}
    {t_i}
    {\STATE_i}
    {v_i}
    \\\\
    \bstepctxgen{m}
    {\STATE_n}
    {\EVALENV,\xctx{x_1}{v_1},\dots,\xctx{x_n}{v_n}}
    {u}
    {\STATE_{n+1}}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE_1}
    {\EVALENV}
    {\explets{x_1}{t_1}{\dots~\expand{x_n}{t_n}}{u}}
    {\STATE_{n+1}}
    {v}
  }

  \inferrule[LetAndRaise]
  {\forall 0 \leq i < k,\quad
    \bstepctx{\STATE_{i-1}}{\EVALENV}{t_i}{\STATE_i}{v_i} \\\\
    \throwctx{\STATE_{k-1}}{\EVALENV}{t_k}{\STATE_k}{v_k} \\
    1 \leq k \leq n
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\explets{x_1}{t_1}{\dots~\expand{x_n}{t_n}}{u}}
    {\STATE_k}
    {v_k}
  }

  \inferrule[Dispatch]
  {\bstepctxgen{m}{\STATE}{\EVALENV}{t}{\STATE'}{v} \\
    \bstepctxgen{m'}{\STATE'}{\EVALENV,\xctx{x_{m}}{v}}{t_{m}}{\STATE''}{v'}}
  {\bstepctxgen{m'}
    {\STATE}
    {\EVALENV}
    {\dispatch{t}~\dval{x_\modeval}{t_\modeval}~|~\dexn{x_\modeexn}{t_\modeexn}}
    {\STATE''}
    {v'}
  }
\end{mathpar}


Alternative rules for let-bindings, that do not enforce the evaluation order:
\begin{mathpar}
  \inferrule[LetAnd']
  {\pi \in \PERMUTATIONS{n} \\
    \forall 0 \leq i \leq n,\quad
    \bstepctx
    {\STATE_{\pi(i-1)}}
    {\EVALENV}
    {t_{\pi(i)}}
    {\STATE_{\pi(i)}}
    {v_{\pi(i)}}
    \\\\
    \bstepctxgen{m}
    {\STATE_{\pi(n)}}
    {\EVALENV,\xctx{x_1}{v_1},\dots,\xctx{x_n}{v_n}}
    {u}
    {\STATE_{n+1}}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE_1}
    {\EVALENV}
    {\explets{x_1}{t_1}{\dots~\expand{x_n}{t_n}}{u}}
    {\STATE_{n+1}}
    {v}
  }

  \inferrule[LetAndRaise']
  {\pi \in \PERMUTATIONS{n} \\
    \forall 0 \leq i < k,\quad
    \bstepctx
    {\STATE_{\pi(i-1)}}
    {\EVALENV}
    {t_{\pi(i)}}
    {\STATE_{\pi(i)}}
    {v_{\pi(i)}}
    \\\\
    \throwctx
    {\STATE_{\pi(k-1)}}
    {\EVALENV}
    {t_{\pi(k)}}
    {\STATE_{\pi(k)}}
    {v_{\pi(k)}}
    \\
    1 \leq k \leq n
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\explets{x_1}{t_1}{\dots~\expand{x_n}{t_n}}{u}}
    {\STATE_{\pi(k)}}
    {v_{\pi(k)}}
  }
\end{mathpar}
where $\PERMUTATIONS{n}$ is the set of permutations on $\{1,\dots,n\}$.

\subsection{Evaluation rules for assertions}

\begin{mathpar}
  \inferrule[AssertTrue]
  {\bstepctx{\STATE}{\EVALENV}{t}{\STATE'}{\mathsf{True}()}}
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\expassert{t}}
    {\STATE'}
    {\mathsf{Unit}()}
  }

  \inferrule[AssertFalse]
  {\bstepctx{\STATE}{\EVALENV}{t}{\STATE'}{\mathsf{False}()}}
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expassert{t}}
    {\STATE'}
    {\mathsf{Assert\_failure}(\mathrm{filename},\mathrm{line},\mathrm{column})}
  }

  \inferrule[AssertRaise]
  {\throwctx{\STATE}{\EVALENV}{t}{\STATE'}{v}}
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expassert{t}}
    {\STATE'}
    {v}
  }
\end{mathpar}

\subsection{Evaluation rules for conditionals}

\begin{mathpar}
  \inferrule[IfTrue]
  {\bstepctx{\STATE}{\EVALENV}{t}{\STATE'}{\mathsf{True}()} \\
    \bstepctxgen{m}{\STATE'}{\EVALENV}{u_1}{\STATE''}{v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\expif{t}{u_1}{u_2}}
    {\STATE''}
    {v}
  }

  \inferrule[IfFalse]
  {\bstepctx{\STATE}{\EVALENV}{t}{\STATE'}{\mathsf{False}()} \\
    \bstepctxgen{m}{\STATE'}{\EVALENV}{u_2}{\STATE''}{v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\expif{t}{u_1}{u_2}}
    {\STATE''}
    {v}
  }

  \inferrule[IfRaise]
  {\throwctx{\STATE}{\EVALENV}{t}{\STATE'}{v}
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expif{t}{u_1}{u_2}}
    {\STATE'}
    {v}
  }
\end{mathpar}

\begin{remark}
  $\mathsf{Assertion\_failure} \in \NAMES$: it is an extension
  constructor.
\end{remark}

\begin{remark}
  The arguments of $\mathsf{Assertion\_failure}$ in the rule
  \textsf{AssertFalse} are the position (\ie file name, line number,
  and column number) of the source file where the assertion failed.
  These data are not modelled in the current document.
\end{remark}

\subsection{Evaluation rules for pattern matching}
\label{sec:sem-pattern-matching}

\newcommand{\Ein}{\EVALENV_{\mathsf{in}}}

Patterns induce a matching relation over values. It is defined by
recursion on patterns, given an initial environment $\Ein$ in which
the values of dynamic constructors are read, and given a matching
environment $\EVALENV$ where the values for each variable of a pattern
are resolved.

\begin{definition}[Matching relation]
\[\begin{array}{r@{~~}c@{~~}l@{~~\eqdef~~}l}
    \matchsub[\Ein]{x &}{& v}{\EVALENV} & \EVALENV(x)=v\\
    \matchsub[\Ein]{b &}{& b}{\EVALENV} & \mathsf{true}\\
    \matchsub[\Ein]{C(p_1,\ldots,p_n) &}{& C(v_1,\ldots,v_n)}{\EVALENV}
      & \bigwedge_{i=1}^n \matchsub[\Ein]{p_i}{v_i}{\EVALENV}\\
    \matchsub[\Ein]{\dync{x}(p_1,\ldots,p_n) &}{ & \dync{\name}(v_1,\ldots,v_n)}{\EVALENV}
      & \Ein(x) = \name \land \bigwedge_{i=1}^n \matchsub[\Ein]{p_i}{v_i}{\EVALENV}\\
    \matchsub[\Ein]{{p_1}\plus{p_2} &}{& v}{\EVALENV}
      & \matchsub[\Ein]{p_1}{v}{\EVALENV} ~\lor~ \matchsub[\Ein]{p_2}{v}{\EVALENV}\\
    \matchsub[\Ein]{{p_1}\minus{p_2} &}{& v}{\EVALENV}
      & \matchsub[\Ein]{p_1}{v}{\EVALENV} ~\land~ \nmatch[\Ein]{p_2}{v}\\
    \matchsub[\Ein]{\bot &}{& v}{\EVALENV} & \mathsf{false}\\
  \end{array}\]
\end{definition}

We say that a pattern $p$ matches a value $v$ given an environment
$\EVALENV$, denoted $\match[\EVALENV]{p}{v}$, when there exists an
environment $\EVALENV'$ such that
$\matchsub[\EVALENV]{p}{v}{\EVALENV'}$. In such case, we write
$\matchctx{p}{v}$ the smallest environment such that
$\matchsub[\EVALENV]{p}{v}{\matchctx{p}{v}}$.

Thanks to this pattern-matching formalism, we can focus on the class
of programs where pattern matching is \emph{exhaustive} and
\emph{non-ambiguous}, \ie: in a term
$\matchwith{t}~ p_1 \earrow u_n \,|\,\cdots\,|\, p_j \earrow u_n$
where $t\sigd\tau$, we require that for any value $v\sigd \tau$, there
exists a unique $1 \leq i \leq n$ such that $\match[ ]{p_i}{v}$. The work
presented in~\cite{Cirstea2019} shows how to \emph{disambiguate}
patterns, \ie, how to make any pattern match non-ambiguous. We
restrict ourselves to non-ambiguous patterns, because it simplifies
both the dynamic semantics and the analysis of programs.


\begin{mathpar}
  \inferrule[Match]
  {\bstepctx{\STATE}{\EVALENV}{t}{\STATE'}{v} \\ \match[\EVALENV]{p_i}{v} \\\\
    \bstepctxgen{m}{\STATE'}{\EVALENV,\matchctx{p_i}{v}}{u_i}{\STATE''}{v'} \\
    1 \leq i \leq n}
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\matchwith{t}~p_1 \earrow u_1 \,|\,\cdots\,|\, p_n \earrow u_n}
    {\STATE''}
    {v'}
  }

  \inferrule[MatchRaise]
  {\throwctx{\STATE}{\EVALENV}{t}{\STATE'}{v}}
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\matchwith{t}~p_1 \earrow u_1 \,|\,\cdots\,|\, p_n \earrow u_n}
    {\STATE'}
    {v}
  }
\end{mathpar}

\subsection{Evaluation rules for creation of dynamic constructors}
\label{sec:sem-dynamic-constructors}

\begin{mathpar}
  \inferrule[DynCreate]
  {\bstepctxgen{m}
    {\STATE \uplus \{\name\}}
    {\EVALENV,\xctx{x}{\name}}{t}
    {\STATE'}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\letdync{x}{t}}
    {\STATE'}
    {v}
  }

  \inferrule[DynAlias]
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV,\xctx{x}{\name}}
    {t}
    {\STATE'}
    {v}
    \and \EVALENV(y) = \name}
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\letdyncalias{x}{y}{t}}
    {\STATE'}
    {v}
  }
\end{mathpar}

\subsection{Evaluation rules for mutable records}
\label{sec:sem-mutable-records}

\begin{mathpar}
  \inferrule[RecordAlloc]
  {\STATE' = \STATE \uplus \{ \loc \mapsto \{ f_1 \mapsto \EVALENV(x_1); \dots; f_n \mapsto \EVALENV(x_n) \} \} }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\{ f_1 = x_1; \dots; f_n = x_n \}}
    {\STATE'}
    {\loc}
  }

  \inferrule[RecordGet]
  {\EVALENV(x) = \loc \\
    \STATE(\loc) = \{ f_1 \mapsto v_1; \dots; f_n \mapsto v_n \} \\
    1 \leq i \leq n
  }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {x.f_i}
    {\STATE}
    {v_i}
  }

  \inferrule[RecordSet]
  {\EVALENV(x) = \loc \\
    \heap(\loc) = \{ f_1 \mapsto v_1; \dots; f_n \mapsto v_n \} \\
    1 \leq i \leq n \\\\
    \STATE' = \STATE \uplus \{ \loc \mapsto \{ f_1 \mapsto v_1; \dots; f_i \mapsto \EVALENV(y); \dots; f_n \mapsto v_n \} \}
  }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {x.f_i \leftarrow y}
    {\STATE'}
    {\mathsf{Unit}()}
  }
\end{mathpar}

\subsubsection{Evaluation rules for loops}

\begin{mathpar}
  \inferrule[ForDone]
  {\EVALENV(y) = n \\
    \EVALENV(z) = m \\
    n = m
  }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\expfor[\mathnormal{dir}]{x}{y}{z}{t}}
    {\STATE}
    {\mathsf{Unit}()}
  }

  \inferrule[ForNext]
  {\EVALENV(y) = n \\
    \EVALENV(z) = m \\
    n \neq m \\
    \bstepctx
    {\STATE}
    {\EVALENV, \xctx{x}{n}}
    {t}
    {\STATE'}
    {\mathsf{Unit}()}
    \\
    (n' = n+1 \land dir = \mathsf{to})
    \lor (n' = n-1 \land dir = \mathsf{downto})
    \\
    \bstepctxgen{m}
    {\STATE'}
    {\EVALENV, \xctx{y}{n'}}
    {\expfor[\mathnormal{dir}]{x}{y}{z}{t}}
    {\STATE''}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\expfor[\mathnormal{dir}]{x}{y}{z}{t}}
    {\STATE''}
    {v}
  }

  \inferrule[ForNextRaise]
  {\EVALENV(y) = n \\
    \EVALENV(z) = m \\
    n \neq m \\
    \throwctx
    {\STATE}
    {\EVALENV, \xctx{x}{n}}
    {t}
    {\STATE'}
    {v}
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expfor[\mathnormal{dir}]{x}{y}{z}{t}}
    {\STATE'}
    {v}
  }

  \inferrule[WhileDone]
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {t}
    {\STATE'}
    {\mathsf{False}()}
  }
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {\expwhile{t}{u}}
    {\STATE'}
    {\mathsf{Unit}()}
  }

  \inferrule[WhileNext]
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {t}
    {\STATE'}
    {\mathsf{True}()}
    \\
    \bstepctx
    {\STATE'}
    {\EVALENV}
    {u}
    {\STATE''}
    {\mathsf{Unit}()}
    \\
    \bstepctxgen{m}
    {\STATE''}
    {\EVALENV}
    {\expwhile{t}{u}}
    {\STATE'''}
    {v}
  }
  {\bstepctxgen{m}
    {\STATE}
    {\EVALENV}
    {\expwhile{t}{u}}
    {\STATE'''}
    {v}
  }

  \inferrule[WhileCondRaise]
  {\throwctx
    {\STATE}
    {\EVALENV}
    {t}
    {\STATE'}
    {v}
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expwhile{t}{u}}
    {\STATE'}
    {v}
  }

  \inferrule[WhileNextRaise]
  {\bstepctx
    {\STATE}
    {\EVALENV}
    {t}
    {\STATE'}
    {\mathsf{True}()}
    \\
    \throwctx
    {\STATE'}
    {\EVALENV}
    {u}
    {\STATE''}
    {v}
  }
  {\throwctx
    {\STATE}
    {\EVALENV}
    {\expwhile{t}{u}}
    {\STATE''}
    {v}
  }
\end{mathpar}

\subsection{Evaluation rules for primitive functions}
\label{sec:sem-primitives}

We only give the semantics of a few primitive functions.

\begin{mathpar}
  \inferrule[PrimRaise]
  { }
  {\throwctx[\kindPrim]
    {\STATE}
    {\EVALENV}
    {\expraise{x}}
    {\STATE}
    {\EVALENV(x)}
  }

  \inferrule[PrimAddInt]
  {\EVALENV(x_1) = n_1 \\
    \EVALENV(x_2) = n_2
  }
  {\bstepctx[\kindPrim]
    {\STATE}
    {\EVALENV}
    {\expcallprim{addInt}{x_1,x_2}}
    {\STATE}
    {n_1 + n_2}
  }

  \inferrule[PrimDivIntNotZero]
  {\EVALENV(x_1) = n_1 \\
    \EVALENV(x_2) = n_2
  }
  {\bstepctx[\kindPrim]
    {\STATE}
    {\EVALENV}
    {\expcallprim{divInt}{x_1,x_2}}
    {\STATE}
    {n_1 / n_2}
  }

  \inferrule[PrimDivIntZero]
  {\EVALENV(x_1) = n_1 \\
    \EVALENV(x_2) = 0
  }
  {\throwctx[\kindPrim]
    {\STATE}
    {\EVALENV}
    {\expcallprim{divInt}{x_1,x_2}}
    {\STATE}
    {\mathsf{Division\_by\_zero}()}
  }
\end{mathpar}

\begin{remark}
  $\mathsf{Division\_by\_zero} \in \NAMES$: it is an extension
  constructor.
\end{remark}


\cleardoublepage
\printbibliography

\end{document}

%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% TeX-master: t
%%% End:

% LocalWords:  monadic functors AST runtime Definitional fixpoint kb
% LocalWords:  unhandled Takeuchi's combinators GADTs polymorphism th
% LocalWords:  skolemisation skolemize boyer multicore equi rcl Const
% LocalWords:  disjunction LetRaise MatchRaise concretisation ccll lr
% LocalWords:  fixpoints Knaster Tarski pointwise contractiveness ary
% LocalWords:  contractive arity unwindings subtyping disjunctions mc
% LocalWords:  Hoare automata Bruijn memoising Kleene Peano monoid de
% LocalWords:  cartesian eval scrutinee ASTs boolean DynamicConstruct
% LocalWords:  LetException RebindException CFA CFAs ccl rcll Alloc
% LocalWords:  GetField SetField locs untyped coercions ascriptions
% LocalWords:  instantiation contravariantly covariantly functor's Yi
% LocalWords:  LoC heintze mcallester tak Fähndrich Pessaux ocamlexc
% LocalWords:  modularly Vilhena indirections hd TRSs memoisation dir
% LocalWords:  Mauborgne Mauborgne's natively contravariance Charlier
% LocalWords:  Hentenryck finalisers Benoît Montagu Lermusiaux downto
% LocalWords:  denumerable booleans Typedtree seqor sequand desugared
% LocalWords:  DynConstruct ExtConstructor AppRaise LetRec LetAnd
% LocalWords:  LetAndRaise AssertTrue AssertFalse AssertRaise IfTrue
% LocalWords:  IfFalse IfRaise DynCreate DynAlias RecordAlloc ForDone
% LocalWords:  RecordGet RecordSet ForNext ForNextRaise WhileDone
% LocalWords:  WhileNext WhileCondRaise WhileNextRaise PrimRaise
% LocalWords:  PrimAddInt addInt PrimDivIntNotZero divInt
% LocalWords:  PrimDivIntZero
