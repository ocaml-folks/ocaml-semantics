# Definitions for Salto's intermediate language

To build the latest version of the document, type `make`.

This will create a file `main.pdf`.

Building the document requires `pdflatex`, `biber` and `latexmk`, that
should be available in your LaTeX installation already.
